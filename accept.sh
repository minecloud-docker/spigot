#!/bin/sh

export accept=$(echo $@)

if [ "$accept" = "accept_eula" ]; then
  echo "Starting Spigot 1.16.5 Server ..."
  echo 'eula=TRUE'>eula.txt
  java -XX:+UseG1GC -jar server.jar nogui 
  pwd
else
  echo 'in case of running server, You Should Accept EULA by adding accept_eula at the end of docker run Command.'
fi

bash