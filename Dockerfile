FROM openjdk:11

EXPOSE 25565:25565

RUN mkdir /root/minecraft
WORKDIR /root/minecraft/

ADD https://download.getbukkit.org/spigot/spigot-1.16.5.jar ./server.jar
COPY accept.sh accept.sh
COPY server.properties server.properties
RUN chmod +x accept.sh

ENTRYPOINT [ "/root/minecraft/accept.sh" ]